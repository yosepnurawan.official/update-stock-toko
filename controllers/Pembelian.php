<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("pembelian_model");
		$this->load->model("supplier_model");
		$this->load->model("barang_model");
	}

	public function index()
	{
		$this->listPembelian();
    }
    
    public function listPembelian()
	{
		$data['data_pembelian']     = $this->pembelian_model->tampilDataPembelian();
        $data['content'] 		    = 'forms/list_pembelian';
		$this->load->view('home', $data);
    }
    
    public function input()
	{
        // panggil data supplier untuk kebutuhan form input
        $data['data_supplier'] 	    = $this->supplier_model->tampilDataSupplier();
        
        // proses simpan ke pembelian header jika ada request form
        if (!empty($_REQUEST)) {
            $pembelian_header = $this->pembelian_model;
            $pembelian_header->savePembelianHeader();
            
            //panggil ID transaksi terakhir
            $id_terakhir = $pembelian_header->idTransaksiTerakhir();
            // var_dump($id_terakhir); die();
            
            // redirect("pembelian/index", "refresh");
            // redirect ke halaman input pembelian detail
			redirect("pembelian/inputdetail/" . $id_terakhir, "refresh");
        }
        
		$data['content'] 		= 'forms/input_pembelian_header';
		$this->load->view('home', $data);
    }
    
    public function inputDetail($id_pembelian_header)
	{
        // panggil data barang untuk kebutuhan form input
        $data['data_barang'] 	= $this->barang_model->tampilDataBarang();
        $data['id_header'] 	    = $id_pembelian_header;
        $data['data_pembelian_detail'] 	= $this->pembelian_model->tampilDataPembelianDetail($id_pembelian_header);
        
        // proses simpan ke pembelian detail jika ada request form
        if (!empty($_REQUEST)) {
            //save detail
            $this->pembelian_model->savePembelianDetail($id_pembelian_header);
            
            //proses update stok
            $kd_barang  = $this->input->post('kode_barang');
            $qty        = $this->input->post('qty');
            $this->barang_model->updateStok($kd_barang, $qty);

			redirect("pembelian/inputdetail/" . $id_pembelian_header, "refresh");
        }
        
		$data['content'] 		= 'forms/input_pembelian_detail';
		$this->load->view('home', $data);
	}
}
