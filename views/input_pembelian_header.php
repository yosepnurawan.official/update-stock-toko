<?php echo validation_errors(); ?>
<div id="container2">
	<center><h1>"Input Pembelian"</h1></center>
    <div id="body" style="text-align: center;">
    <form action="<?=base_url()?>pembelian/input" method="POST">
    <table width="100%" border="0" cellspacing="0" cellpadding="3" style="margin: 0 auto;">
    <tr>
        <td width="45%" style="text-align: right;">Nomor Transaksi</td>
        <td width="3%">:</td>
        <td width="52%" style="text-align: left;">
            <input type="text" id="no_transaksi" name="no_transaksi">
        </td>
    </tr>
	<tr>
        <td style="text-align: right;">Nama Supplier</td>
        <td>:</td>
        <td style="text-align: left;">
            <select id="kode_supplier" name="kode_supplier">
                <?php foreach($data_supplier as $data) { ?>
                <option value="<?= $data->kode_supplier; ?>"><?= $data->nama_supplier; ?></option>
                <?php }?>
            </select>
        </td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td style="text-align: left;">
        <input type="submit" value="simpan" name="simpan" /> 
        <input type="reset" value="reset" name="reset" />
        <br/>
        <br/>
        <a href="<?= base_url(); ?>pembelian/index">
            <input type="button" value="Kembali Ke Menu Sebelumnya" name="kembali" />
        </a>
        </td>
	</tr>
</table>
</form>
</div>
	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>