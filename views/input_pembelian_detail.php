<?php echo validation_errors(); ?>
<div id="container2">
	<center><h1>"Input Pembelian Detail"</h1></center>
    <div id="body" style="text-align: center;">
    <form action="<?=base_url()?>pembelian/inputDetail/<?= $id_header; ?>" method="POST">
    <table width="100%" border="0" cellspacing="0" cellpadding="3" style="margin: 0 auto;">
	<tr>
        <td style="text-align: right;">Nama Barang</td>
        <td>:</td>
        <td style="text-align: left;">
            <select id="kode_barang" name="kode_barang">
                <?php foreach($data_barang as $data) { ?>
                <option value="<?= $data->kode_barang; ?>"><?= $data->nama_barang; ?></option>
                <?php }?>
            </select>
        </td>
    </tr>
    <tr>
        <td width="45%" style="text-align: right;">Qty</td>
        <td width="3%">:</td>
        <td width="52%" style="text-align: left;">
            <input type="text" id="qty" name="qty">
        </td>
    </tr>
    <tr>
        <td width="45%" style="text-align: right;">Harga Barang</td>
        <td width="3%">:</td>
        <td width="52%" style="text-align: left;">
            <input type="text" id="harga" name="harga">
        </td>
    </tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td style="text-align: left;">
        <input type="submit" value="proses" name="proses" />         
        </td>
	</tr>
</table>
</form><br/>

<table width="100%" border="1" cellspacing="0" cellpadding="3">
	<tr align="center" style="background:#000; color:#FFF;">
		<td width="3%">No</td>
		<td width="15%">Kode Barang</td>
		<td width="22%">Nama Barang</td>
		<td width="20%">Qty</td>
		<td width="24%">Harga</td>
		<td width="24%">Jumlah</td>
    </tr>
    <?php
    // var_dump($data_pembelian_detail); die();
        $no = 0;
        $total_hitung = 0;
        foreach ($data_pembelian_detail as $data) {
            $no++;
    ?>
	<tr align="center">
		<td><?= $no; ?></td>
		<td><?= $data->kode_barang; ?></td>
		<td><?= $data->nama_barang; ?></td>
		<td><?= $data->qty; ?></td>
		<td align="right">Rp. <?= number_format($data->harga); ?> ,-</td>
		<td  align="right">Rp. <?= number_format($data->jumlah); ?> ,-</td>
    </tr>
    <?php
        // hitung total
        $total_hitung += $data->jumlah;
        } 
    ?>
    <tr align="center">
		<td colspan="5" align="right"><b>TOTAL</b></td>
		<td  align="right">Rp. <b><?= number_format($total_hitung); ?></b></td>
    </tr>
    </table>
    <br/>
        <a href="<?= base_url(); ?>pembelian/index">
            <input type="button" value="Kembali Ke Menu Sebelumnya" name="kembali" />
        </a>
</div>
	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>