<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_model extends CI_Model
{
    //panggil nama table
    private $_table = "barang";

    // public function tampilDataBarang()
    // {
    //     // seperti : select * from <nama_table>
    //     return $this->db->get($this->_table)->result();
    // }

    public function tampilDataBarang()
    {
        $query	= $this->db->query(
            "SELECT barang.*, 
                jenis_barang.nama_jenis 
                FROM barang INNER JOIN  jenis_barang 
                ON barang.kode_jenis = jenis_barang.kode_jenis 
                WHERE barang.flag = 1"
        );
        return $query->result();	
    }
    
    public function detail($kode_barang)
    {
        $query	= $this->db->query("SELECT barang.*, jenis_barang.nama_jenis FROM barang INNER JOIN  jenis_barang ON barang.kode_jenis = jenis_barang.kode_jenis WHERE barang.flag = 1 AND barang.kode_barang = '$kode_barang'");
        return $query->result();
    }

    public function tampilDataBarang2()
    {
        $query	= $this->db->query("SELECT * FROM barang WHERE flag = 1");
        return $query->result();	
    }

    public function detail2($kode_barang)
    {
        $query	= $this->db->query("SELECT * FROM barang WHERE flag = 1 AND kode_barang = '$kode_barang'");
        return $query->result();	
    }

    public function save()
    {
        $data['kode_barang']   = $this->input->post('kode_barang');
        $data['nama_barang']   = $this->input->post('nama_barang');
        $data['harga_barang']     = $this->input->post('harga_barang');
        $data['kode_jenis']     = $this->input->post('kode_jenis');
        $data['stok']           = $this->input->post('stok');
        $data['flag']           = 1;
        $this->db->insert($this->_table, $data);
    }

    public function update($kode_barang)
    {
        $data['kode_barang']   = $this->input->post('kode_barang');
        $data['nama_barang']   = $this->input->post('nama_barang');
        $data['harga_barang']  = $this->input->post('harga_barang');
        $data['kode_jenis']    = $this->input->post('kode_jenis');
        $data['flag']          = 1;
        
        $this->db->where('kode_barang', $kode_barang);
        $this->db->update($this->_table, $data);
    }

    public function delete($kode_barang)
    {
        //soft delete
        // $data['flag']           = 0;        
        // $this->db->where('kode_barang', $kode_barang);
        // $this->db->update($this->_table, $data);

        //delete from db
        $this->db->where('kode_barang', $kode_barang);
        $this->db->delete($this->_table);
    }

    public function updateStok($kd_barang, $qty)
    {
        //panggil data detail stok
        $cari_stok = $this->detail($kd_barang);
        foreach ($cari_stok as $data) {
            $stok = $data->stok;
        }
        
        //proses update stok table barang
        $jumlah_stok     = $stok + $qty;
        $data_barang['stok']    = $jumlah_stok;
        
        $this->db->where('kode_barang', $kd_barang);
        $this->db->update('barang', $data_barang);
    }

}